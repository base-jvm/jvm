#!/bin/bash
echo "export LANG=en_US.UTF-8" >> /etc/profile
source /etc/profile

mkdir -p /logs/$LOG_PREFIX/`hostname` /data/logs
ln -s /logs/$LOG_PREFIX/`hostname` $LOG_HOME

JAVA_OPTS="$JAVA_OPTS -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=1 -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true -verbose:gc -Xloggc:$LOG_HOME/gc.log "

echo -e "Starting the Server ...\c"
mkdir -p $LOG_HOME
echo "" >>$LOG_HOME/gc.log
java $JAVA_OPTS -jar /data/$JAR_NAME --server.port=$PORT