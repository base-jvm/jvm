package com.rofth.basejvm.jvm;

/**
 * @author: chengchao
 * @description:
 * @date: create in 2021/4/23
 **/
public class EdgenJvmTest {
    private static final int _1MB = 1024 * 1024;

    /***
     * 方法说明：本方法只针对校验edge区域内存不足时，发生Minor GC后，Survivor也不足，这批对象是否分配到Old
     *
     *
     * 1、什么时候触发Minor GC?
     *    当新生代的Eden区和其中一个Survivor区空间不足时。
     * 2、Minor GC前先判断
     * q1：存活的对象所占的内存空间 < Survivor区域内存空间的大小， 那么存活的对象进入Survivor区
     * q2：Survivor区域内存空间大小 < 存活的对象所占的内存空间 < 老年代的可用空间大小。那么存活的对象进入到老年代
     * q3：(存活的对象所占的内存空间 >Survivor区域内存空间的大小) && (存活的对象所占的内存空间 > 老年代的可用空间)，那么会触发Full GC,老年代腾出空间后，再进行Minor GC。
     *     如果腾出空间后还不能存放活的对象，那么会导致OOM（堆内存空间不足）
     *
     *
     * jvm参数：-Xms20M  -Xmx20M  -Xmn10M  -XX:SurvivorRatio=8  -XX:+PrintGCDetails
     *         堆大小            年轻代大小
     *
     * 发生GC时，发现3个2M的对象无法全部存放在存活区（1M）,从而会将allocation1, allocation2, allocation3担保到老年代
     */
    public static void object_EdgeMemoryAllocation_对象优先在edge中分配() {
        byte[] allocation1, allocation2, allocation3, allocation4;
        allocation1 = new byte[2 * _1MB];
        allocation2 = new byte[2 * _1MB];
        allocation3 = new byte[2 * _1MB]; //出现一次Minor GC
        allocation4 = new byte[4 * _1MB];
    }

    /***
     * 方法说明：本方法只针对超过设定的值得对象 直接进入到老年代
     *-----------------------------------------------------------------------------------------------------------------
     *
     * jvm参数：-Xms20M  -Xmx20M  -Xmn10M  -XX:SurvivorRatio=8  -XX:+PrintGCDetails -XX:PretenureSizeThreshold=3145728
     *         堆大小            年轻代大小
     *
     * 发生GC时，发现3个2M的对象无法全部存放在存活区（1M）,从而会将allocation1, allocation2, allocation3担保到老年代
     */
    public static void object_bigObjectAllocation_old_大对象直接分配到老年代() {
        byte[] allocation1, allocation2, allocation3, allocation4;
        allocation1 = new byte[1 * _1MB];
        allocation2 = new byte[1 * _1MB];
        allocation3 = new byte[1 * _1MB];
        allocation4 = new byte[4 * _1MB]; //直接分配到老年代
    }

    public static void main(String[] args) {
        //object_EdgeMemoryAllocation_对象优先在edge中分配();
        object_bigObjectAllocation_old_大对象直接分配到老年代();
    }
}
