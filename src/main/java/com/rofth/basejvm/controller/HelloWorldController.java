package com.rofth.basejvm.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

/**
 * @author: chengchao
 * @description:
 * @date: create in 2021/3/4
 **/
@RestController
public class HelloWorldController {

    @GetMapping("/hello")
    public String hello() {
        return "您好，HELLOWORLD";
    }

    public static void main(String[] args) {
        String s = "bid_real_day_20210403";
        System.out.println(s.split("_")[3]);
        dateConvertion(s.split("_")[3]);
    }

    public static void dateConvertion(String str) {
        try {
            Date format1 = new SimpleDateFormat("yyyyMMdd").parse(str);
            String longDate = new SimpleDateFormat("yyyy-MM-dd").format(format1);
            System.out.println("yyyyMMdd转yyyy-MM-dd:" + longDate);
        } catch (ParseException e) {
        }
    }

}
