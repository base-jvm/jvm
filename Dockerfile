FROM java:8
ADD target/*.jar app.jar
EXPOSE 8080
#相当于在容器中用cmd命令执行jsr包，并制定外部配置文件
ENTRYPOINT ["java", "-jar", "/app.jar"]